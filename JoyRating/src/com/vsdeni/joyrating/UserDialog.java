package com.vsdeni.joyrating;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

public class UserDialog extends Activity implements OnClickListener {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.user_dialog);
		findViewById(R.id.btnOk).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				EditText etUser = (EditText) findViewById(R.id.etUserName);
				SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
				editor.putString("user", etUser.getText().toString());
				editor.commit();
				finish();

				int ids[] = AppWidgetManager.getInstance(getApplication()).getAppWidgetIds(
						new ComponentName(getApplication(), com.vsdeni.joyrating.JoyWidgetProvider.class));

				Intent intent = new Intent(getApplicationContext(), JoyWidgetService.class);
				intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);
				startService(intent);
			}
		}).start();
	}
}
