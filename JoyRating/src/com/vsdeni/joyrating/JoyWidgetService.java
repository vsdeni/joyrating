package com.vsdeni.joyrating;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;

import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

public class JoyWidgetService extends Service {
	private static final String RATING_TAG = "<div id=\"rating-text\">";
	private static final String PROGRESS_TAG = "poll_res_bg_active";
	private static final String STARS_TAG = "<div class=\"stars\"";
	private static final String URL = "http://joyreactor.cc/";

	private String getUser() {
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		return sharedPreferences.getString("user", "");
	}

	private int convertStringToDouble(String var) {
		int result = -1;
		try {
			result = Integer.parseInt(var);
		} catch (Exception e) {
		}
		return result;
	}

	private Karma getRating() {

		String user = getUser();
		Karma karma = null;
		if (!"".equals(user)) {
			String url = null;
			try {
				url = URL + "user/" + URLEncoder.encode(user, HTTP.UTF_8);
			} catch (UnsupportedEncodingException e1) {
				e1.printStackTrace();
			}
			if (url != null) {
				Log.d("url", url);
				HttpGet httpget = new HttpGet(url);
				ResponseHandler<String> responseHandler = new BasicResponseHandler();
				HttpClient client = new DefaultHttpClient();
				String result = "";
				try {
					result = client.execute(httpget, responseHandler);
				} catch (Exception e) {
					e.printStackTrace();
				}

				String progress = "";
				String rating = "";

				if (!"".equals(result)) {
					String starTag = "star-";

					int stars = 0;
					for (int i_s = result.indexOf(STARS_TAG) + STARS_TAG.length(); i_s > starTag.length(); i_s = result.indexOf(starTag, i_s)
							+ starTag.length()) {
						String type = result.substring(i_s, i_s + 1);
						if ("0".equals(type) || "1".equals(type)) {
							stars++;
						}
					}

					int i_pro = result.indexOf(PROGRESS_TAG) + PROGRESS_TAG.length();
					int i_pro_1 = result.indexOf(":", i_pro) + 1;
					int i_pro_2 = result.indexOf("%", i_pro_1);
					progress = result.substring(i_pro_1, i_pro_2);

					int i_rat = result.indexOf(RATING_TAG, i_pro_2) + RATING_TAG.length();
					int i_rat_1 = result.indexOf(">", i_rat) + 1;
					int i_rat_2 = result.indexOf("<", i_rat_1);
					rating = result.substring(i_rat_1, i_rat_2);

					karma = new Karma();
					karma.setRating(rating);
					karma.setProgress(convertStringToDouble(progress));
					karma.setStars(stars);
				}

			}
		}
		return karma;
	}

	private void setOnUpdateClickListener(RemoteViews remoteViews, int[] appWidgetIds) {
		Intent intent = new Intent(getApplicationContext(), JoyWidgetProvider.class);
		intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
		intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		remoteViews.setOnClickPendingIntent(R.id.widget, pendingIntent);
	}

	private void setOnOpenSiteClickListener(RemoteViews remoteViews) {
		Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(URL));
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, 0);
		remoteViews.setOnClickPendingIntent(R.id.btnRefresh, pendingIntent);
	}

	private void setOnSelectUserClickListener(RemoteViews remoteViews) {
		Intent clickIntent = new Intent(getApplicationContext(), UserDialog.class);
		clickIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, clickIntent, 0);
		remoteViews.setOnClickPendingIntent(R.id.tvUserName, pendingIntent);
	}

	private void showPreloader(RemoteViews remoteViews) {
		remoteViews.setViewVisibility(R.id.tvRating, View.INVISIBLE);
		remoteViews.setViewVisibility(R.id.pbUpdateRating, View.VISIBLE);
	}

	private void hidePreloader(RemoteViews remoteViews) {
		remoteViews.setViewVisibility(R.id.tvRating, View.VISIBLE);
		remoteViews.setViewVisibility(R.id.pbUpdateRating, View.INVISIBLE);
	}

	private void showKarma(RemoteViews remoteViews) {
		Karma karma = getRating();
		if (karma != null) {
			String rating = karma.getRating();
			remoteViews.setTextViewText(R.id.tvRating, rating);
			int progress = karma.getProgress();
			remoteViews.setProgressBar(R.id.pbProgress, 100, progress, false);
			int stars = karma.getStars();
			Log.d("stars", String.valueOf(stars));
			for (int i = 0; i < 10; i++) {
				int id = getApplicationContext().getResources().getIdentifier("star" + i, "id", getApplicationContext().getPackageName());
				if (i < stars) {
					remoteViews.setImageViewResource(id, R.drawable.star_active);
				} else {
					remoteViews.setImageViewResource(id, R.drawable.star_inactive);
				}
				if (i > 0 && stars > 10) {
					remoteViews.setViewVisibility(id, View.INVISIBLE);
				} else {
					remoteViews.setViewVisibility(id, View.VISIBLE);
				}
			}
			if (stars > 10) {
				remoteViews.setViewVisibility(R.id.tvStarsCount, View.VISIBLE);
				remoteViews.setTextViewText(R.id.tvStarsCount, " x " + stars);
			} else {
				remoteViews.setViewVisibility(R.id.tvStarsCount, View.INVISIBLE);
			}
		}
	}

	private void showUser(RemoteViews remoteViews) {
		String user = getUser();
		if (user.equals("")) {
			user = getString(R.string.user_not_selected);
		}
		remoteViews.setTextViewText(R.id.tvUserName, user);
	}

	@Override
	public int onStartCommand(final Intent intent, int flags, int startId) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(JoyWidgetService.this.getApplicationContext());
				int[] allWidgetIds = intent.getIntArrayExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS);
				for (int widgetId : allWidgetIds) {
					RemoteViews remoteViews = new RemoteViews(JoyWidgetService.this.getApplicationContext().getPackageName(), R.layout.widgetlayout);

					showUser(remoteViews);

					showPreloader(remoteViews);
					appWidgetManager.updateAppWidget(widgetId, remoteViews);

					showKarma(remoteViews);

					setOnUpdateClickListener(remoteViews, allWidgetIds);
					setOnSelectUserClickListener(remoteViews);
					setOnOpenSiteClickListener(remoteViews);

					hidePreloader(remoteViews);
					appWidgetManager.updateAppWidget(widgetId, remoteViews);
				}
				stopSelf();
			}
		}).start();

		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

}